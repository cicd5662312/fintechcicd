package com.samanecorporation.metier.sevice;

import com.samanecorporation.metier.dao.AccountRepository;
import com.samanecorporation.metier.dto.AccountDto;
import com.samanecorporation.metier.entity.AccountEntity;
import com.samanecorporation.metier.exception.EntityNotFoundException;
import com.samanecorporation.metier.mapper.AccountMapper;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AccuntService implements IAccountService {

    private AccountRepository accountRepository;
    private AccountMapper accountMapper;
    private MessageSource messageSource;

    @Override
    public Optional<AccountDto> save(AccountDto accountDto) {
        AccountEntity accountEntity = accountRepository.save(accountMapper.toAccountEntity(accountDto));
        return Optional.of(accountMapper.toAccountDto(accountEntity));
    }

    @Override
    public Optional<AccountDto> get(String number) {

        return accountRepository.findByNumber(number)
                .map(accountEntity -> Optional.of(accountMapper.toAccountDto(accountEntity)))
                .orElseThrow(
                        () -> new EntityNotFoundException(
                                messageSource.getMessage("account.notfound",
                                                                new Object[]{number},
                                                            Locale.getDefault()
                                                         )
                                )
                );
    }

    @Override
    public List<AccountDto> findAll() {
        return List.of();
    }
}
