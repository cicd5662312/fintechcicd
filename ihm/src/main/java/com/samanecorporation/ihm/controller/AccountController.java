package com.samanecorporation.ihm.controller;

import com.samanecorporation.metier.dto.AccountDto;
import com.samanecorporation.metier.sevice.IAccountService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;

@Controller
@AllArgsConstructor
public class AccountController {

    private IAccountService accountService;

    @GetMapping(name = "home", value = "/")
    public String home(Model model) {
        Optional<AccountDto> accountDto = accountService.save(
                AccountDto.builder()
                        .number("BISFSRD1324")
                        .amount(50000.0)
                        .currency("CFA")
                        .build()
        );
        accountDto.ifPresent(account -> model.addAttribute("account", account));

        return "home";
    }
}
